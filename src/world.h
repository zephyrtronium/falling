//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#pragma once

#include "graphics.h"
#include "material.h"
#include "rng.h"

// world is a matrix of particles. It is opaque to ensure the implementation
// is easy to change.
struct world;

// new_world creates an empty world of the given size which knows the given
// materials.
struct world *new_world(struct material *mats, int num_mats, int width, int height);
// free_world releases all resources associated with the world and frees it.
void free_world(struct world *world);

// world_size gets the width and height of non-border cells in the world.
// Calling world_at on the same world with 0 <= row < height and
// 0 <= col < width will not return MATERIAL_BORDER.
void world_size(const struct world *world, int *width, int *height);
// world_at gets the material at a single position. If the position is out of
// bounds, the result is MATERIAL_BORDER.
material_t world_at(const struct world *world, int row, int col);

// world_draw draws a world onto the graphics system. This moves the update
// space to the current space and so should occur after all physics work.
// For testing purposes, gfx may be NULL to perform the world buffer swap.
void world_draw(struct graphics *gfx, const struct world *world);

// world_interactions processes all interactions in the world.
void world_interactions(struct world *world, struct rng *rng);
// world_move moves all particles in the world.
void world_move(struct world *world, struct rng *rng);

// world_fill fills every cell in the world with a given material.
void world_fill(struct world *world, material_t mat);
// world_paint_circle fills cells within a radius of r of a given coordinate.
void world_paint_circle(struct world *world, int row, int col, double r, material_t mat);
// world_paint_rect fills cells inside the rectangle given by (row1, col1) at
// the top left to (row2, col2) at the bottom right.
void world_paint_rect(struct world *world, int row1, int col1, int row2, int col2, material_t mat);
