//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdlib.h>

#include "graphics.h"
#include "rng.h"
#include "world.h"

int main(int argc, char **argv) {
    struct world *world = NULL;
    struct graphics gfx;
    struct rng rng;

    int frame_skip = 1;

    if (init_graphics(&gfx, 640, 480)) {
        goto cleanup;
    }
    if ((world = new_world(NULL, 0, 480, 480)) == NULL) {
        goto cleanup;
    }
    rng_init(&rng, 1ULL);

    // call some paint functions for now so we know the calls complete
    world_fill(world, 1);
    world_paint_circle(world, 0, 0, 10.0, 2);
    world_paint_rect(world, 500, 500, 300, 300, 2);

    do {
        for (int i = 0; i < frame_skip; i++) {
            world_interactions(world, &rng);
            world_move(world, &rng);
        }
        world_draw(&gfx, world);
    } while(!graphics_frame(&gfx));

cleanup:
    graphics_end(&gfx);
    free_world(world);
    return EXIT_SUCCESS;
}
