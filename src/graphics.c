//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdlib.h>

#include "graphics.h"

int init_graphics(struct graphics *gfx, int win_width, int win_height) {
    if (!glfwInit()) {
        goto error;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    gfx->win = glfwCreateWindow(win_width, win_height, "falling", NULL, NULL);
    if (gfx->win == NULL) {
        goto error;
    }
    glfwMakeContextCurrent(gfx->win);
    glfwSwapInterval(1);

    return 0;

error:
    int err = glfwGetError(NULL);
    graphics_end(gfx);
    return err;
}

int graphics_blit(const struct graphics *gfx, material_t *world, int width, int height, float (*palette)[3]) {
    // TODO: this
    return 0;
}

int graphics_frame(const struct graphics *gfx) {
    if (glfwWindowShouldClose(gfx->win)) {
        return 1;
    }
    glfwSwapBuffers(gfx->win);
    glfwPollEvents();
    return 0;
}

void graphics_request_end(const struct graphics *gfx) {
    glfwWindowShouldClose(gfx->win);
}

void graphics_end(struct graphics *gfx) {
    glfwDestroyWindow(gfx->win);
    glfwTerminate();
}
