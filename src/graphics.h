//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#pragma once

#include <GLFW/glfw3.h>

#include "material.h"

// graphics encapsulates graphics management, including UI and drawing.
struct graphics {
    // win is the GLFW window for the game. The game should set callbacks,
    // especially for input, on win.
    GLFWwindow *win;
};

// init_graphics initializes the graphics system. Returns nonzero on failure.
int init_graphics(struct graphics *gfx, int win_width, int win_height);

// graphics_blit draws the world into the viewable graphical region. width and
// height give the size of the entire world, regardless of the size of the
// region in view. The palette is the colors of each material by ordinal; it
// must be at least as long as the greatest material value.
int graphics_blit(const struct graphics *gfx, material_t *world, int width, int height, float (*palette)[3]);

// graphics_frame renders a frame to the screen, possibly waiting for vblank.
// It additionally triggers GLFW input handling callbacks.
// The return value is nonzero if the graphics system should end.
int graphics_frame(const struct graphics *gfx);

// graphics_request_end requests that the graphics system gracefully exit.
void graphics_request_end(const struct graphics *gfx);

// graphics_end releases all resources relating to the graphics system. This
// should be called only after graphics_request_end has returned and all other
// graphics operations have finished.
void graphics_end(struct graphics *gfx);
