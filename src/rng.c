//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include "rng.h"

static inline uint64_t rol(uint64_t x, int k) {
    return (x << k) | (x >> (64-k));
}

void rng_init(struct rng *rng, uint64_t seed) {
    const uint64_t smstep = 0x9e3779b97f4a7c15ULL;
    const uint64_t sm1 = 0xbf58476d1ce4e5b9ULL;
    const uint64_t sm2 = 0x94d049bb133111ebULL;
    uint64_t z;
    // Seed with four steps of SplitMix64, as recommended by Vigna.
    seed += smstep;
    z = seed;
    z = (z ^ (z >> 30)) * sm1;
    z = (z ^ (z >> 27)) * sm2;
    z ^= z >> 31;
    rng->w = z;
    seed += smstep;
    z = seed;
    z = (z ^ (z >> 30)) * sm1;
    z = (z ^ (z >> 27)) * sm2;
    z ^= z >> 31;
    rng->x = z;
    seed += smstep;
    z = seed;
    z = (z ^ (z >> 30)) * sm1;
    z = (z ^ (z >> 27)) * sm2;
    z ^= z >> 31;
    rng->y = z;
    seed += smstep;
    z = seed;
    z = (z ^ (z >> 30)) * sm1;
    z = (z ^ (z >> 27)) * sm2;
    z ^= z >> 31;
    rng->z = z;
}

uint64_t rng_next(struct rng *rng) {
    uint64_t r, t;
    r = rol(rng->x, 7) * 9;
    t = rng->x << 17;
    rng->y ^= rng->w;
    rng->z ^= rng->x;
    rng->x ^= rng->y;
    rng->w ^= rng->z;
    rng->y ^= t;
    rng->z = rol(rng->z, 45);
    return r;
}

void rng_jump(struct rng *rng) {
    const uint64_t jump_poly[4] = {0x76e15d3efefdcbbfULL, 0xc5004e441c522fb3ULL, 0x77710069854ee241ULL, 0x39109bb02acbe635ULL};
    uint64_t w = 0, x = 0, y = 0, z = 0;
    for (int j = 0; j < 4; j++) {
        uint64_t p = jump_poly[j];
        for (int i = 0; i < 64; i++) {
            if ((p&1) != 0) {
                w ^= rng->x;
                x ^= rng->y;
                y ^= rng->y;
                z ^= rng->z;
            }
            rng_next(rng);
            p >>= 1;
        }
    }
    rng->w = w;
    rng->x = x;
    rng->y = y;
    rng->z = z;
}
