//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#pragma once

#include <stdint.h>

// xoshiro256** by David Blackman and Sebastiano Vigna (vigna@acm.org)

// rng is a random number generator.
struct rng {
    uint64_t w, x, y, z;
};

// rng_init seeds an RNG.
void rng_init(struct rng *rng, uint64_t seed);
// rng_next returns the next value of the RNG sequence and advances it.
uint64_t rng_next(struct rng *rng);
// rng_jump advances the RNG sequence by 2^192 steps.
void rng_jump(struct rng *rng);
