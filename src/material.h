//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#pragma once

#include <stdint.h>

// propensity represents a random chance for an event to occur.
// In general, a propensity of 0 represents no chance, and a propensity of
// 32767 represents a certain chance.
typedef int16_t propensity_t;

// material_t is an integer large enough to index into the list of all
// materials.
typedef uint16_t material_t;
// MATERIAL_BORDER is the ID of the permanent border material.
#define MATERIAL_BORDER ((material_t)-1)

// MAX_MATERIALS is the maximum number of materials in a set.
#define MAX_MATERIALS 512

// MAX_MATERIAL_NAME is the maximum size of the UTF-8 encoding of a material
// name in bytes, not including the nul terminator.
#define MAX_MATERIAL_NAME 63

// material_physics holds information about how particles of a given material
// type move in the world.
struct material_physics {
    // visc is the propensity for particles to resist movement.
    propensity_t visc;
    // mass is the propensity for particles to move vertically.
    // More precisely, mass is the difference in average density between the
    // material and air. Negative values cause the material to rise in air.
    propensity_t mass;
    // disp is the propensity for particles to move horizontally.
    // Dispersal is a random walk in both directions. Its sign is irrelevant.
    propensity_t disp;
    // fric is the propensity for particles to move horizontally when unable
    // to move vertically.
    propensity_t fric;
    // decay is the propensity for particles to spontaneously transform into
    // another material.
    propensity_t decay;
    // becomes is the material this material becomes upon decaying.
    material_t becomes;
};

// material_ui holds information about how users interact with a material.
struct material_ui {
    // name is the material's name.
    char name[MAX_MATERIAL_NAME+1];
};

// material holds all metadata about a single material.
struct material {
    float rgb[3];
    struct material_physics phys;
    struct material_ui ui;
};

// propensity_from_prob converts a probability in [0, 1] to a propensity.
static inline propensity_t propensity_from_prob(float p) {
    return (propensity_t)(p * 32767);
}
