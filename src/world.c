//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdlib.h>
#include <string.h>

#include "world.h"

struct world {
    // frags is the content of the world.
    material_t *frags;
    // update is the next content of the world.
    material_t *update;
    // w and h are the size of the world.
    int w, h;
    // pitch is the number of cells between rows of the rendered portion of
    // memory owned by the world.
    int pitch;

    // world converts the physics of all materials from AoS to SoA.
    float (*rgb)[3];
    propensity_t *visc, *mass, *disp, *fric, *decay;
    material_t *becomes;
};

static inline size_t world_memsize(const struct world *world) {
    return (size_t)world->pitch * (size_t)world->h * sizeof(material_t);
}

struct world *new_world(struct material *mats, int num_mats, int width, int height) {
    struct world *world = NULL;
    size_t wsz = (size_t)width * (size_t)height;

    if (width <= 0 || height <= 0) {
        return NULL;
    }

    // Allocate the world and its members in just a few blocks.
    // Use calloc for the world so all the pointers are already null and we can
    // just free them if an alloc fails.
    // Keep the allocations in sync with free_world.
    world = calloc(sizeof(struct world), 1);
    if (world == NULL) {
        return NULL;
    }
    world->frags = calloc(wsz*2 + num_mats, sizeof(material_t));
    if (world->frags == NULL) {
        goto error;
    }
    world->update = world->frags + wsz;
    world->becomes = world->update + wsz;
    world->rgb = malloc(num_mats*3 * sizeof(float));
    if (world->rgb == NULL) {
        goto error;
    }
    world->visc = malloc(num_mats*5 * sizeof(propensity_t));
    if (world->visc == NULL) {
        goto error;
    }
    world->mass = world->visc + num_mats;
    world->disp = world->mass + num_mats;
    world->fric = world->disp + num_mats;
    world->decay = world->fric + num_mats;

    // Copy data into the arrays.
    for (int i = 0; i < num_mats; i++) {
        struct material mat = mats[i];
        world->rgb[i][0] = mat.rgb[0];
        world->rgb[i][1] = mat.rgb[1];
        world->rgb[i][2] = mat.rgb[2];
        world->visc[i] = mat.phys.visc;
        world->mass[i] = mat.phys.mass;
        world->disp[i] = mat.phys.disp;
        world->fric[i] = mat.phys.fric;
        world->decay[i] = mat.phys.decay;
        world->becomes[i] = mat.phys.becomes;
    }

    world->w = width;
    world->h = height;
    world->pitch = width;

    return world;

error:
    free_world(world);
    return NULL;
}

void free_world(struct world *world) {
    // new_world consolidates allocations. Keep in sync.
    free(world->frags);
    free(world->rgb);
    free(world->visc);
    free(world);
}

void world_size(const struct world *world, int *width, int *height) {
    *width = world->w;
    *height = world->h;
}

material_t world_at(const struct world *world, int row, int col) {
    if (row < 0 || row >= world->h || col < 0 || col >= world->w) {
        return MATERIAL_BORDER;
    }
    return world->frags[(size_t)row * world->w + col];
}

void world_draw(struct graphics *gfx, const struct world *world) {
    size_t sz = world_memsize(world);
    memmove(world->frags, world->update, sz);
    memset(world->update, 0, sz);
    if (gfx != NULL) {
        graphics_blit(gfx, world->frags, world->w, world->h, world->rgb);
    }
}

void world_interactions(struct world *world, struct rng *rng) {
    // unimplemented
}

void world_move(struct world *world, struct rng *rng) {
    // unimplemented
}

void world_fill(struct world *world, material_t mat) {
    for (int row = 0; row < world->h; row++) {
        for (int col = 0; col < world->w; col++) {
            world->update[row*world->pitch + col] = mat;
        }
    }
}

void world_paint_circle(struct world *world, int row, int col, double r, material_t mat) {
    double r2 = r*r;
    for (int irow = row - (int)r - 1; irow < row + (int)r + 1; irow++) {
        if (irow < 0 || irow >= world->h) {
            continue;
        }
        double dy = (double)(row - irow);
        dy *= dy;
        for (int icol = col - (int)r - 1; icol < col + (int)r + 1; icol++) {
            if (icol < 0 || icol >= world->w) {
                continue;
            }
            double dx = (double)(col - icol);
            dx *= dx;
            if (dy + dx <= r2) {
                world->update[irow*world->pitch + icol] = mat;
            }
        }
    }
}

void world_paint_rect(struct world *world, int row1, int col1, int row2, int col2, material_t mat) {
    // Rectify and clip the rectangle.
    if (row2 < row1) {
        int t = row2;
        row2 = row1;
        row1 = t;
    }
    if (row1 < 0) {
        row1 = 0;
    }
    if (row1 >= world->h) {
        return;
    }
    if (row2 < 0) {
        return;
    }
    if (row2 > world->h) {
        row2 = world->h;
    }
    if (col2 < col1) {
        int t = col2;
        col2 = col1;
        col1 = t;
    }
    if (col1 < 0) {
        col1 = 0;
    }
    if (col1 >= world->w) {
        return;
    }
    if (col2 < 0) {
        return;
    }
    if (col2 > world->h) {
        col2 = world->h;
    }

    for (int row = row1; row < row2; row++) {
        for (int col = col1; col < col2; col++) {
            world->update[row*world->pitch + col] = mat;
        }
    }
}
