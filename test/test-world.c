//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdbool.h>

#include "test.h"

#include "material.h"
#include "world.h"

// same_world returns true when the world has the given width and height and
// has contents matching mats.
static bool same_world(const struct world *world, const material_t mats[], int width, int height) {
    int w_w, w_h;
    world_size(world, &w_w, &w_h);
    if (w_w != width || w_h != height) {
        return false;
    }
    for (int row = 0; row < height; row++) {
        for (int col = 0; col < width; col++) {
            material_t want = mats[row*width + col];
            material_t got = world_at(world, row, col);
            if (want != got) {
                return false;
            }
        }
    }
    return true;
}

// test_new_world_size tests that a size reports the same size that was passed
// to create it.
static bool test_new_world_size() {
    const int width = 2, height = 2;
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }

    int got_w, got_h;
    world_size(world, &got_w, &got_h);
    if (got_w != width) {
        fprintf(stderr, FAILMSG("wrong width: want %d, got %d"), width, got_w);
        ok = false;
    }
    if (got_h != height) {
        fprintf(stderr, FAILMSG("wrong height: want %d, got %d"), height, got_h);
        ok = false;
    }

    free_world(world);
    return ok;
}

// test_new_world_content tests that a new world is all blank inside.
static bool test_new_world_content() {
    const int width = 2, height = 2;
    const material_t want[2*2] = {
        0, 0,
        0, 0,
    };
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }

    if (!same_world(world, want, width, height)) {
        fprintf(stderr, FAILMSG("worlds differ"));
        ok = false;
    }

    free_world(world);
    return ok;
}

// test_new_world_border tests that a new world is surrounded by MATERIAL_BORDER.
static bool test_new_world_border() {
    const int width = 2, height = 2;
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }

    // Check top border.
    for (int col = -1; col < width + 1; col++) {
        if (world_at(world, -1, col) != MATERIAL_BORDER) {
            fprintf(stderr, FAILMSG("top border has wrong material at col %d"), col);
            ok = false;
        }
    }
    // Check bottom border.
    for (int col = -1; col < width + 1; col++) {
        if (world_at(world, height, col) != MATERIAL_BORDER) {
            fprintf(stderr, FAILMSG("bottom border has wrong material at col %d"), col);
            ok = false;
        }
    }
    // Check left border.
    for (int row = -1; row < height + 1; row++) {
        if (world_at(world, row, -1) != MATERIAL_BORDER)  {
            fprintf(stderr, FAILMSG("left border has wrong material at row %d"), row);
            ok = false;
        }
    }
    // Check right border.
    for (int row = -1; row < height + 1; row++) {
        if (world_at(world, row, width) != MATERIAL_BORDER)  {
            fprintf(stderr, FAILMSG("right border has wrong material at row %d"), row);
            ok = false;
        }
    }

    free_world(world);
    return ok;
}

static bool test_fill() {
    const int width = 3, height = 3;
    const material_t want[3*3] = {
        1, 1, 1,
        1, 1, 1,
        1, 1, 1,
    };
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }
    world_fill(world, 1);
    world_draw(NULL, world); // flip buffers

    if (!same_world(world, want, width, height)) {
        fprintf(stderr, FAILMSG("world not filled"));
        ok = false;
    }

    free_world(world);
    return ok;
}

static bool test_circle() {
    const int width = 5, height = 5;
    const material_t want[5*5] = {
        1, 1, 1, 0, 0,
        1, 1, 1, 1, 0,
        1, 1, 1, 1, 0,
        1, 1, 1, 1, 0,
        1, 1, 1, 0, 0,
    };
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }
    world_paint_circle(world, 2, 1, 2.3, 1);
    world_draw(NULL, world); // flip buffers

    if (!same_world(world, want, width, height)) {
        fprintf(stderr, FAILMSG("wrong paint"));
        ok = false;
    }

    free_world(world);
    return ok;
}

static bool test_rect() {
    const int width = 8, height = 8;
    const material_t want[8*8] = {
        3, 0, 0, 0, 0, 0, 0, 4,
        0, 0, 2, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 5, 5,
        6, 0, 0, 0, 0, 0, 5, 5,
    };
    bool ok = true;

    struct world *world = new_world(NULL, 0, width, height);
    if (world == NULL) {
        fprintf(stderr, ERRORMSG("couldn't create world with size %dx%d"), width, height);
        return false;
    }
    world_paint_rect(world, 3, 3, 5, 5, 1);
    world_paint_rect(world, 2, 3, 1, 2, 2);
    world_paint_rect(world, 1, 1, -1, -1, 3);
    world_paint_rect(world, -1, 7, 1, height+1, 4);
    world_paint_rect(world, 6, 6, height+1, width+1, 5);
    world_paint_rect(world, 7, -1, height, 1, 6);
    world_paint_rect(world, -2, -2, 0, width+1, 7);
    world_paint_rect(world, -2, width, height+2, width+2, 8);
    world_draw(NULL, world); // flip buffers

    if (!same_world(world, want, width, height)) {
        fprintf(stderr, FAILMSG("wrong paint"));
        ok = false;
    }

    free_world(world);
    return ok;
}


int main(int argc, char **argv) {
    START_TESTS
    RUN_TEST(test_new_world_size)
    RUN_TEST(test_new_world_content)
    RUN_TEST(test_new_world_border)
    RUN_TEST(test_fill)
    RUN_TEST(test_circle)
    RUN_TEST(test_rect)
    FINISH
}
