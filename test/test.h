//  Copyright 2022 Branden J Brown
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//      http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>

// FAILMSG produces a standardized test failure message.
#define FAILMSG(msg) "- FAIL: " msg "\n"
// ERRORMSG produces a standardized test error message. Test errors are
// failures which prevent the test function from performing any meaningful
// testing, such as failing to allocate resources.
#define ERRORMSG(msg) "- ERROR: " msg "\n"

// START_TESTS performs initial setup to begin running tests in main.
#define START_TESTS int test_total_count = 0, test_fail_count = 0;
// RUN_TEST runs the named test function, which should return false on failure.
// Each test run must happen in the same function as START_TESTS and FINISH.
#define RUN_TEST(name) {\
    test_total_count++;\
    puts("RUN: " # name);\
    if (!name()) {\
        test_fail_count++;\
        puts("FAIL: " # name);\
    }\
}
// FINISH reports test results and exits the executable.
#define FINISH {\
    if (test_fail_count != 0) {\
        printf("%d tests ran, %d failed\n", test_total_count, test_fail_count);\
        exit(EXIT_FAILURE);\
    }\
    puts("PASS");\
    exit(EXIT_SUCCESS);\
}
