# Falling Tests

Each file here is a separate test executable. The makefile requires that all tests be named like `test-*.c`. `test.h` provides some facilities for writing tests.
