# Derived from the Makefile recipe at https://makefiletutorial.com.

TARGET_EXEC := falling

BUILD_DIR := ./build
SRC_DIRS := ./src
TEST_SRC_DIR := ./test
TEST_DIR := $(BUILD_DIR)/test

# Enumerate non-main source files.
SRCS := \
	src/graphics.c \
	src/rng.c \
	src/world.c

# The source file containing the entry point.
# This is separate to make building tests easier.
MAINSRC := src/main.c

# Enumerate test programs. Each must correspond to a .c file in $TEST_SRC_DIR.
TESTS := \
	test-world

CFLAGS := -std=c11 -Wall -Werror -O2 -g $(shell pkg-config --cflags glfw3) $(CFLAGS)
LDFLAGS := $(shell pkg-config --static --libs glfw3) $(LDFLAGS)

# String substitution for every source file.
# As an example, hello.c turns into ./build/hello.c.o
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
MAINOBJ := $(MAINSRC:%=$(BUILD_DIR)/%.o)

# String substitution (suffix version without %).
# As an example, ./build/hello.cpp.o turns into ./build/hello.cpp.d
DEPS := $(OBJS:.o=.d)

# Every folder in ./src will need to be passed to GCC so that it can find header files
INC_DIRS := $(SRC_DIRS)
# Add a prefix to INC_DIRS. So moduleA would become -ImoduleA. GCC understands this -I flag
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

# The -MMD and -MP flags together generate Makefiles for us!
# These files will have .d instead of .o as the output.
CFLAGS := $(INC_FLAGS) -MMD -MP $(CFLAGS)

# The final build step.
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS) $(MAINOBJ)
	$(CC) $(OBJS) $(MAINOBJ) -o $@ $(LDFLAGS)

# Build step for C source
$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

# Build for tests.
$(TEST_DIR)/%: $(TEST_SRC_DIR)/%.c $(OBJS)
	mkdir -p $(TEST_DIR)
	$(CC) $(CFLAGS) $< $(OBJS) -o $@ $(LDFLAGS)

# Run a test. For reasons I cannot identify, make removes the executable after
# this, so also move it to build/ on failure.
test-%: $(TEST_DIR)/test-%
	$(TEST_DIR)/$@ || mv $(TEST_DIR)/$@ $(BUILD_DIR)

.PHONY: test
test: $(TESTS)

.PHONY: clean
clean:
	-rm -r $(BUILD_DIR)

.PHONY: all
all: $(BUILD_DIR)/$(TARGET_EXEC) test

# Include the .d makefiles. The - at the front suppresses the errors of missing
# Makefiles. Initially, all the .d files will be missing, and we don't want those
# errors to show up.
-include $(DEPS)
